import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:rapidor_test/app_textfield.dart';
import 'package:rapidor_test/custome_board.dart';
import 'package:rapidor_test/pincode_screen_controller.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  final controller = Get.put(PicodeScreenController());

  MyHomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Pin Code Page"),
      ),
      body: Form(
        key: controller.formKey,
        child: Center(
          child: Column(
            children: [
              CustomBoard(backspace: () {
                controller.controlBackspace();
              }, input: (val) {
                controller.controlInput(val);
              }),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  for (int i = 0; i < controller.focusNodes.length; i++)
                    Expanded(
                        child: AppTextField(
                      validator: (value) => controller.pincodeValidator(),
                      onFocusChange: (val) {
                        if (val == true) {
                          controller.textFieldIndex = i;
                        }
                      },
                      textEditingController:
                          controller.textEditingControllers[i],
                      focusNode: controller.focusNodes[i],
                    ))
                ],
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
          onPressed: controller.verifyPin, child: const Text("Verify")),
    );
  }
}
