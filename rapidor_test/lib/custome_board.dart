import 'package:flutter/material.dart';

class CustomBoard extends StatelessWidget {
  const CustomBoard({super.key, required this.backspace, required this.input});

  final Function(String) input;
  final Function() backspace;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            _buildButton('1'),
            _buildButton('2'),
            _buildButton('3'),
          ],
        ),
        Row(
          children: [
            _buildButton('4'),
            _buildButton('5'),
            _buildButton('6'),
          ],
        ),
        Row(
          children: [
            _buildButton('7'),
            _buildButton('8'),
            _buildButton('9'),
          ],
        ),
        Row(
          children: [
            _buildButton(''),
            _buildButton('0'),
            _buildButton('⌫', onPressed: backspace),
          ],
        ),
      ],
    );
  }


  Widget _buildButton(String text, {VoidCallback? onPressed}) {
    return Expanded(
      child: TextButton(
        onPressed: onPressed ?? () => input(text),
        child: Text(text),
      ),
    );
  }
}
