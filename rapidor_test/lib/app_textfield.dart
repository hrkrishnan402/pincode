import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class AppTextField extends StatelessWidget {
  AppTextField(
      {this.onChanged,
      this.focusNode,
      required this.textEditingController,
      required this.onFocusChange,
      this.validator,
      super.key});

  final FocusNode? focusNode;
  final Function(String?)? onChanged;
  final TextEditingController textEditingController;
  final Function(bool) onFocusChange;
  Function(String?)? validator;

  final border = const OutlineInputBorder(
    borderSide: BorderSide(width: 3, color: Colors.greenAccent), //<-- SEE HERE
  );

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 50,
      width: 20,
      child: FocusScope(
        onFocusChange: ((value) => onFocusChange(value)),
        child: TextFormField(
          showCursor: true,
          validator: (value) => validator!(value),
          readOnly: true,
          controller: textEditingController,
          inputFormatters: [
            LengthLimitingTextInputFormatter(1),
          ],
          focusNode: focusNode,
          onChanged: onChanged,
          decoration:
              InputDecoration(focusedBorder: border, enabledBorder: border),
        ),
      ),
    );
  }
}
