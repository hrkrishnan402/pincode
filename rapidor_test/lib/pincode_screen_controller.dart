import 'package:flutter/material.dart';
import 'package:get/get.dart';

class PicodeScreenController extends GetxController {
  int textFieldIndex = 0;
  final formKey = GlobalKey<FormState>();
  List<FocusNode> focusNodes = [
    FocusNode(),
    FocusNode(),
    FocusNode(),
    FocusNode(),
  ];

  List<TextEditingController> textEditingControllers = [
    TextEditingController(),
    TextEditingController(),
    TextEditingController(),
    TextEditingController(),
  ];

  final pinArray = [
    "Empty",
    "Empty",
    "Empty",
    "Empty",
  ];

  void controlInput(String val) {
    textEditingControllers[textFieldIndex].text = val;
    manageTextFields(textFieldIndex, val);
  }

  void controlBackspace() {
    textEditingControllers[textFieldIndex].text = "";
    if (textFieldIndex != 0) {
      focusNodes[textFieldIndex - 1].requestFocus();
    }
  }

  manageTextFields(int i, String? val) {
    if ((val ?? "").isNotEmpty) {
      pinArray[i] = val!;
    } else {
      pinArray[i] = "Empty";
    }
    if (i < focusNodes.length - 1 && (val ?? "").isNotEmpty) {
      textFieldIndex = i + 1;
      focusNodes[i + 1].requestFocus();
    }
  }

  pincodeValidator() {
    if (textEditingControllers.map((e) => e.text).toList().join("") != "1234") {
      return "";
    }
    return null;
  }

  verifyPin(){
    if (formKey.currentState!.validate()) {
              Get.snackbar("Success", "Correct PIN",
                  backgroundColor: Colors.green);
            } else {
              Get.snackbar("Failed", "Wrong PIN", backgroundColor: Colors.red);
            }
  }
}
